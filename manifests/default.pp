$nextcloud_version = '18.0.1'
$nextcloud_instance_name = 'example'
$nextcloud_docroot_instance1 = "/var/nextcloud-${nextcloud_instance_name}-${nextcloud_version}"
$nextcloud_docroot = "${nextcloud_docroot_instance1}/nextcloud"
$nextcloud_dbname_instance1 = "nextcloud_${nextcloud_instance_name}"
$nextcloud_dbuser_instance1 = "user_${nextcloud_instance_name}"
$nextcloud_dbpasswd = 'secret'

$www_user = 'www-data'
$mariadb_client = 'mariadb-client-10.1'
$mariadb_server = 'mariadb-server'

package { ['cron']:
  ensure => present,
}

class { 'apache':
  mpm_module    => 'prefork',
  default_vhost => false,
  default_mods  => ['php','rewrite','headers','env','dir','mime'],
  manage_user   => false,
  manage_group  => false,
  require       => File["${nextcloud_docroot_instance1}"],
}
apache::vhost { $facts['networking']['fqdn'] :
  servername    => $facts['networking']['fqdn'],
  port          => '80',
  docroot       => "${nextcloud_docroot}",
  docroot_owner => "${www_user}",
  docroot_group => "${www_user}",
  directories   => [
    {
      path => "${nextcloud_docroot}",
      allow_override => 'All',
    },
  ],
}

class { 'mysql::server':
  package_name     => "${mariadb_server}",
  service_name     => 'mysql',
  override_options => {
    mysqld => {
      bind_address          => '127.0.0.1',
      transaction_isolation => 'READ-COMMITTED',
      binlog_format         => 'ROW',
      innodb_large_prefix   => true,
      innodb_file_format    => 'barracuda',
      innodb_file_per_table => '1',
    },
  },
  root_password    => 'secretsecret',
}

class { 'mysql::client':
  package_name => "${mariadb_client}",
}

mysql::db { "${nextcloud_dbname_instance1}" :
  user     => "${nextcloud_dbuser_instance1}",
  password => "${nextcloud_dbpasswd}",
  host     => 'localhost',
  grant    => ['ALL'],
  require  => [
    Class['mysql::server'],
  ]
}

include nextcloud

nextcloud::instance { 'example' :
  database_name     => 'nextcloud_example',
  database_user     => 'user_example',
  database_password => 'secret',
  admin_password    => 'secret',
  system_user       => 'www-data',
  config            => {
    trusted_domains => [
      'localhost',
      'cloud.example.com',
    ],
  }
}

